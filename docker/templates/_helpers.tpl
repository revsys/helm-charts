{{/* vim: set filetype=sls sw=2 ts=2: */}}


{{- define "InitMethod" -}}

  {{- $major := .Capabilities.KubeVersion.Major -}}
  {{- $minor := .Capabilities.KubeVersion.Minor -}}

  {{- if and (lt (int $major) 2) (lt (int $minor) 8) }}
    {{- printf "annotation" -}}
  {{- else -}}

    {{- if (eq (int $major) 1) and (ge (int $minor) 8) }}
      {{- printf "spec" -}}
    {{- end -}} {{/* else if */}}

  {{- end -}} {{/* if */}}
{{- end -}} {{/* define */}}

